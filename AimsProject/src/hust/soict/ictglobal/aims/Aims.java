package hust.soict.ictglobal.aims;

import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.aims.order.Order;

public class Aims {
	public static void showMenu() {
			System.out.println("Order Management Application: ");
			System.out.println("--------------------------------");
			System.out.println("1. Create new order");
			System.out.println("2. Add item to the order");
			System.out.println("3. Delete item by id");
			System.out.println("4. Display the items list of order");
			System.out.println("0. Exit");
			System.out.println("--------------------------------");
			System.out.println("Please choose a number: 0-1-2-3-4");
	}
		public static void main(String[] args) {
			Scanner keyboard = new Scanner(System.in);
			int choose;
			ArrayList<Order> orded= new ArrayList<Order>();
			do {
				showMenu();
				choose = keyboard.nextInt();
				
				switch(choose)
				{
				case 1:
					Order demo = new Order();
					orded.add(demo);
					System.out.println("order have been add");
					break;
				case 2:
					System.out.println("input title:");
					String title = "";
					Media med = new Media(1,title);
					orded.get(0).addMedia(med);
					break;
				case 3:
					System.out.println("input id");
					int id = keyboard.nextInt();
					orded.get(0).deleteByid(id);
					break;
				case 4:
					orded.get(0).print();
					break;
				}
			}while(choose != 0);	
			keyboard.close();
}
}
		
		
		

	
