package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media{
	private List<String> authors = new ArrayList<String>();
	public boolean addAuthor(String authorName) {
		if(this.authors.indexOf(authorName)>=0) {
			System.out.println("The author is present");
			return false;
		}
		this.authors.add(authorName);
		System.out.println("The author have been add");
		return true;
	}
	public boolean removeAuthor(String authorName) {
		while(this.authors.indexOf(authorName)<0) {
			System.out.println("The author is not already");
			return false;
		}
		this.authors.remove(authorName);
		System.out.println("The author have been remove");
		return true;
	}
	public List<String> getAuthors() {
		return authors;
	}
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	public Book(int id,String title) {
		super(id,title);
	}

}
