package hust.soict.ictglobal.aims.media;

public class DigitalVideoDisc extends Media{
	public DigitalVideoDisc(int id, String title) {
		super(id,title);
	}
	private String director;
	private int length;
	public boolean search(String title) {
		return (this.getTitle().indexOf(title)>=0)? true : false;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
}

